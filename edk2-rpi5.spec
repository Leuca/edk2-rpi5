%define debug_package %{nil}
%bcond_with debug

%global edk2_commit eca8aaebb3ec6e4707a8e5765dd372feb5bbee6e

# Build options
%global TOOL_CHAIN_TAG GCC5
%global TARGET_ARCH AARCH64
%global ACTIVE_PLATFORM Platform/RaspberryPi/RPi5/RPi5.dsc
%global DEFAULT_KEYS -D DEFAULT_KEYS=TRUE -D PK_DEFAULT_FILE=$WORKSPACE/keys/pk.cer -D KEK_DEFAULT_FILE1=$WORKSPACE/keys/ms_kek1.cer -D KEK_DEFAULT_FILE1=$WORKSPACE/keys/ms_kek2.cer -D DB_DEFAULT_FILE1=$WORKSPACE/keys/ms_db1.cer -D DB_DEFAULT_FILE2=$WORKSPACE/keys/ms_db2.cer -D DB_DEFAULT_FILE1=$WORKSPACE/keys/ms_db3.cer -D DB_DEFAULT_FILE1=$WORKSPACE/keys/ms_db4.cer -D DBX_DEFAULT_FILE1=$WORKSPACE/keys/arm64_dbx.bin
%global BUILD_FLAGS -D SECURE_BOOT_ENABLE=TRUE -D INCLUDE_TFTP_COMMAND=TRUE -D NETWORK_ISCSI_ENABLE=TRUE -D SMC_PCI_SUPPORT=1
%if %{with debug}
%global TARGET DEBUG
%else
%global TARGET RELEASE
%endif

Name:           edk2-rpi5
Version:        0.3

%global WORKSPACE %{_builddir}/%{name}-%{version}

Release:        3%{?dist}
Summary:        UEFI firmware for Raspberry Pi 5
License:        BSD-2-Clause-Patent
URL:            https://github.com/worproject/rpi5-uefi
Source0:        config.txt
Source1:        99-vc4.conf

ExclusiveArch:	aarch64

# Enable more ACPI+Devicetree by default
Patch0:         edk2-rpi5-defaults.patch


BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  python3
BuildRequires:  git
BuildRequires:  libuuid-devel
BuildRequires:  acpica-tools
BuildRequires:  openssl
BuildRequires:  efi-filesystem
BuildRequires:  efi-srpm-macros
BuildRequires:  arm-trusted-firmware-armv8

Requires:       bcm2712-firmware

%description
This is a port of 64-bit Tiano Core UEFI firmware for the Raspberry Pi 5 platform.

This is intended to be useful 64-bit TF-A + UEFI implementation for the Raspberry Pi variants based on the BCM2711 SoC, which should be good enough for most kind of UEFI development, as well running consumer Operating Systems.

Raspberry Pi is a trademark of the Raspberry Pi Foundation.

%prep
mkdir -p %{WORKSPACE}

%build
pushd %{WORKSPACE}
    export WORKSPACE=%{WORKSPACE}
    git clone https://github.com/worproject/edk2.git
    pushd edk2
        git checkout %{edk2_commit}
        git submodule update --init
    popd

    git clone https://github.com/worproject/edk2-platforms.git
    pushd edk2-platforms
        git submodule update --init
        %{__patch} -p1 < %{PATCH0}
    popd

    git clone https://github.com/tianocore/edk2-non-osi.git

    export PACKAGES_PATH=$PWD/edk2:$PWD/edk2-platforms:$PWD/edk2-non-osi

    . edk2/edksetup.sh

    %{__make} -C edk2/BaseTools

    # Now before building set up Secure Boot default keys
    # From https://github.com/pftf/RPi4/blob/master/.github/workflows/linux_edk2.yml
    mkdir keys
    openssl req -new -x509 -newkey rsa:2048 -subj "/CN=Raspberry Pi 5 Platform Key/" -keyout /dev/null -outform DER -out keys/pk.cer -days 7300 -nodes -sha256
    curl -L https://go.microsoft.com/fwlink/?LinkId=321185 -o keys/ms_kek1.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=2239775 -o keys/ms_kek2.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=321192 -o keys/ms_db1.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=321194 -o keys/ms_db2.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=2239776 -o keys/ms_db3.cer
    curl -L https://go.microsoft.com/fwlink/?linkid=2239872 -o keys/ms_db4.cer
    curl -L https://uefi.org/sites/default/files/resources/dbxupdate_arm64.bin -o keys/arm64_dbx.bin

    build -n $(nproc) \
        -b %{TARGET} \
        -a %{TARGET_ARCH} \
        -t %{TOOL_CHAIN_TAG} \
        -p %{ACTIVE_PLATFORM} \
        -D TFA_BUILD_ARTIFACTS=%{_datadir}/arm-trusted-firmware/rpi5 \
        --pcd gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor=L"EDK II for Raspberry Pi 5" \
        --pcd gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString=L"UEFI for RPI5 %{version}" \
        %{BUILD_FLAGS} %{DEFAULT_KEYS}
popd

%install
pushd %{WORKSPACE}
    mkdir -p %{buildroot}%{efi_esp_root}
    install -m 0600 Build/RPi5/%{TARGET}_GCC5/FV/RPI_EFI.fd %{buildroot}%{efi_esp_root}/
    install -m 0600 %{SOURCE0} %{buildroot}%{efi_esp_root}/
    # Text files
    install -m 0644 edk2-platforms/License.txt %{_builddir}
    install -m 0644 edk2-platforms/Maintainers.txt %{_builddir}
    install -m 0644 edk2-platforms/Readme.md %{_builddir}
    install -m 0644 edk2-platforms/License-History.txt %{_builddir}
popd

# X11 Configuration for vc4
mkdir -p %{buildroot}%{_sysconfdir}/X11/xorg.conf.d
install %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xorg.conf.d

%files
%config(noreplace) %{efi_esp_root}/config.txt
%license License.txt
%doc Maintainers.txt Readme.md License-History.txt
%{efi_esp_root}/RPI_EFI.fd
%{_sysconfdir}/X11/xorg.conf.d/99-vc4.conf

%changelog
* Tue Sep 24 2024 Luca Magrone <luca@magrone.cc> 0.3-3
- Embed the additional 2023 Secure Boot certificates from Microsoft

* Sun May 05 2024 Luca Magrone <luca@magrone.cc> 0.3-2
- Add X11 conf for vc4
- Enable full vc4 capability in config.txt

* Sun May 05 2024 Luca Magrone <luca@magrone.cc> 0.3-1
- Initial version of the package
